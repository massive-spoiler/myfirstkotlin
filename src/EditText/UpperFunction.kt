package wthumbs

object UpperFunction {
	class Result(
			val word: String,
			val index: Int,
			val length: Int,
			val appendSpace: Boolean,
			val cursorMove: Boolean)

	/// in->In->IN->in
	/// 頭大文字、全大文字、最初に戻る。
	/// 末尾ならSpaceを、次がスペースでその次もスペースならカーソル移動を行う
	fun DoN(text: String, cursor: Int): Result? {
		var r = WordGetter.getWordN(text, cursor);
		if (r == null) return null;

		var word = text.substring(r.index, r.length);
		if (word.toLowerCase() == word) {
			//全部小文字
			word = UpperFirst(word);
			return R(word, r);
		} else if (word.toUpperCase() == word) {
			//全部大文字
			word = word.toLowerCase();
			return R(word, r);
		} else {
			word = word.toUpperCase();
			return R(word, r);
		}
	}

	fun UpperFirst(word: String) = Character.toUpperCase(word[0]) + word.substring(1);

	fun R(word: String, r: WordGetter.Result): Result {
		return Result(
				word,
				r.index,
				r.length,
				r.appendSpace,
				r.cursorMove
		)
	}
}
