package wthumbs

class StrResult(
	val startIndex : Int,
	val length : Int)
{
	//JavaのendIndexは最後の文字のindexの次のようなんだなあ
	val endIndex : Int get() = startIndex + length
}