package wthumbs



object WordGetter {
	class Result(
			val index: Int,
			val length: Int,
			val appendSpace: Boolean,
			val cursorMove: Boolean)

	/// <summary>
	/// カーソルのひとつ前がスペースや記号ならひとつ先を探す。ひとつ先もなければ2つ前を探す。
	/// </summary>
	fun getWordN(text: String, cursor: Int): Result? {
		var wrapped = StrWrapper(text);

		var word = wrapped.searchWordN(cursor - 1);
		if (word != null) return R(word, wrapped);

		word = wrapped.searchWordN(cursor);
		if (word != null) return R(word, wrapped);

		word = wrapped.searchWordN(cursor - 2);
		if (word != null) return R(word, wrapped);

		return null;
	}

	fun R(r: StrResult, wrapped: StrWrapper): Result {
		var appendSpace = false;
		var cursorMove = false;
		//末尾ならスペースを追加するのが推奨される
		var c = wrapped.getCharN(r.startIndex + r.length);
		if (c == null) {
			appendSpace = true;
		} else {
			var prevChar = c;
			c = wrapped.getCharN(r.startIndex + r.length + 1);
			if (c == null || Character.isWhitespace(c)) {
				if (Character.isWhitespace(prevChar)) {
					//単語の次の次が末尾かスペースで単語の次がスペースならカーソルを右に動かすのが推奨される
					cursorMove = true;
				}
			}
		}
		return Result(
				r.startIndex,
				r.length,
				appendSpace,
				cursorMove)
	}
}
