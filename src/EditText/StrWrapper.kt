package wthumbs

class StrWrapper(val text : String) {
	fun getLetterN(index: Int): Char? {
		val c = getCharN(index);
		if (c != null && isLetter(c)) {
			return c;
		} else return null;
	}

	fun getCharN(index: Int): Char? {
		if (0 <= index && index < text.length) {
			return text[index];
		}
		return null;
	}

	fun searchWordN(charIndex : Int) : StrResult?
	{
		if (getLetterN(charIndex) == null) return null;

		var startIndex = charIndex;
		var endIndex = charIndex;
		for (i in (charIndex + 1) until text.length)
		{
			if (isLetter(text[i])) {
				endIndex = i;
			} else break;
		}
		for (i in (charIndex - 1) downTo 0)
		{
			if (isLetter(text[i])) {
				startIndex = i;
			} else break;
		}
		return StrResult(startIndex, endIndex-startIndex+1);
	}


	companion object {
		fun isLetter(c : Char) : Boolean
		{
			return CharList.isWordChar(c);
		}
	}

}