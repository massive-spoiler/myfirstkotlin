package wthumbs

import java.nio.file.Files
import java.nio.file.Paths

enum class Langu
{
	En, Fr, De, Es, Pt
}

object CandidateDictCreator
{
	fun LangToFile(lang:Langu) : String
	{
		return when(lang)
		{
			Langu.En ->  "en";
			Langu.Fr -> "fr";
			Langu.De -> "de";
			Langu.Es -> "es";
			Langu.Pt -> "pt";
			else -> throw IllegalArgumentException();
		}
	}

	fun Do(itaiji : Itaiji, lang : Langu /*, myDict : MyDictionary*/) : CandidateDict
	{
		val lines = Files.readAllLines(Paths.get("res/Dict/${LangToFile(lang)}.txt"), Charsets.UTF_8);
		//var lines = myDict.Merge(lines_);

		val temp = CandidateDictTemp();

		for (i in lines.indices)
		{
			val line = lines[i];
			val comparison = itaiji.comparisonStr(line);
			temp.Add(WordItem(line, comparison, i))
		}

		return temp.toCandidateDict(itaiji);
	}
}