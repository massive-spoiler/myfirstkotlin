package wthumbs

class WordItem(
	val word : String,
    val comparison : String,
    freq : Int) : Comparable<WordItem>
{
	/// lower is better
	val freq = freq

	override fun toString() = "$word $freq $comparison";

	override fun compareTo(other : WordItem) : Int
	{
		return this.comparison.compareTo(other.comparison);
	}
}