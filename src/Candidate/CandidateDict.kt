package wthumbs

import java.util.*

class CandidateDict(
		val sorted : Array<WordItem>,
        val itaiji : Itaiji)
{
	companion object{ val MaxCandidates = 4; }

	fun getWordList(original : String) : Array<String>
	{
		val comparison = itaiji.comparisonStr(original);
		if (comparison.length == 0) return arrayOf<String>();

		val input = WordItem(original, comparison, 0);
		var index = Arrays.binarySearch(sorted, input);
		if (index < 0) { index = index.inv() }

		val result = ArrayList<WordItem>();
		var hasSame = false;
		for (i in index until (sorted.size))
		{
			var item = sorted[i];
			if (item.comparison.startsWith(comparison))
			{
				result.add(item);
				if (item.comparison.length == comparison.length)
				{
					hasSame = true;
				}
			}
			else break;
		}
		result.sortBy{ it.freq }
		var freqWords = result.map{ it.word };

		var upperTested = freqWords.filter{ upperTest(it, original) }
		var upperFirst = upperTested
				.map{ if(Character.isUpperCase(original[0])) upperFirst(it) else it }

		if (hasSame)
		{
			//その単語が存在する場合、大文字化した候補を用意しておく
			var upper = UpperFunction.DoN(original, 0)!!.word;

			//var same = arrayOf(upper);
			//大文字化した候補がある場合はオリジナルの候補は出さない
			var filtered = upperFirst.filter{ it != original && it != upper }
			//末尾にくっつける
			return filtered.take(MaxCandidates - 1).plus(upper).toTypedArray();
		}
		else
		{
			return upperFirst.take(MaxCandidates).toTypedArray();
		}
	}

	/// 二文字目以降が大文字の場合、大文字でない候補は切る。
	fun upperTest(word : String, original : String) : Boolean
	{
		for (i in 1 until original.length)
		{
			if (Character.isUpperCase(original[i]))
			{
				if (Character.isUpperCase(word[i]) == false)
				{
					return false;
				}
			}
		}
		return true;
	}

	fun upperFirst(word : String) : String
	{
		return Character.toUpperCase(word[0]) + word.substring(1);
	}

}