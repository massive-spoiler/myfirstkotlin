package wthumbs

import java.util.*

class CandidateDictTemp
{
	val list = ArrayList<WordItem>();

	fun Add(item : WordItem)
	{
		list.add(item);
	}

	fun toCandidateDict(itaiji : Itaiji) : CandidateDict
	{
		list.sort()
		return CandidateDict(list.toTypedArray(), itaiji);
	}
}