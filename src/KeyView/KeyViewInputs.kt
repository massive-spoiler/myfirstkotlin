package wthumbs

import java.util.*

/// <summary>
/// 左上から右に向かって一段二段三段と進めていく。今の想定だと4*4の16オンリーだ。
/// </summary>
class KeyViewInputs(
		val items : Array<KeyViewInput>)
{
	val candidatePushing = Array(CandidateDict.MaxCandidates, {false})

	companion object {
		val length = 16;

		fun createEmpty16() : KeyViewInputs
		{
			return KeyViewInputs(Array(length, {KeyViewInput.createEmpty() }))
		}
	}

	fun deepCopy() : KeyViewInputs
	{
		return KeyViewInputs(items.map{ it.deepCopy() }.toTypedArray())
	}

	operator fun get(side : SideBtn, quad : QuadBtn) = items[getIndex(side,quad)]
	operator fun set(side : SideBtn, quad : QuadBtn, value : KeyViewInput) { items[getIndex(side,quad)] = value }


	fun getIndex(side : SideBtn, quad : QuadBtn) = KeyViewDefaultVisualizerTemp.getIndex(side, quad);
}


class KeyViewInput(
		val i5 : Array<KeyViewArranger.Input>,
        var pushing : Boolean) {
	fun deepCopy(): KeyViewInput {
		return KeyViewInput(i5.map { it.deepCopy() }.toTypedArray(), pushing);
	}

	companion object {
		fun createEmpty(): KeyViewInput {
			return KeyViewInput(Array(5, { KeyViewArranger.Input.EMPTY }), false)
		}

		fun getCentered(text: String, width: Int, pushing: Boolean): KeyViewInput {
			var empty = KeyViewArranger.Input.EMPTY;
			return KeyViewInput(
					arrayOf(
							KeyViewArranger.Input(text, width, FontSize.Big),
							empty, empty, empty, empty),
					pushing);
		}
	}
}