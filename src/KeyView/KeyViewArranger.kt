package wthumbs

object KeyViewArranger
{
	fun Arrange(rate : Rate, i5 : Array<Input>, width : Int, height : Int) : Result
	{
		var w = width; var h = height;
		var bigCharSize = rate.bigCharSize;
		var smallCharSize = rate.smallCharSize;
		var smallCharRate = rate.smallCharRate;
		var border = rate.borderRate;

		var bigX = w * 0.5 - i5[0].width / 2;
		var bigY = h * 0.5 - bigCharSize / 2;
		var borderW = w * border;
		var borderH = h * border;

		//SmallCharの左側中心軸　中心をはみ出すほどWidthが大きい場合はずらす
		var leftC = borderW + w * smallCharRate / 2;
		var rightC = w - borderW - w*smallCharRate / 2;
		var bottomY = h - borderH - Math.max(
				h * smallCharRate / 2 + smallCharSize,
				smallCharSize);

		var luX = leftC - i5[1].width / 2;
		luX = Math.max(borderW, luX);
		var luY = borderH;

		var ruX = rightC - i5[2].width / 2;
		if (ruX + i5[2].width > w - borderW)
		{
			ruX = w - borderW - i5[2].width;
		}
		var ruY = borderH;

		var ldX = leftC - i5[4].width / 2;
		ldX = Math.max(borderW, ldX);
		var ldY = bottomY;

		var rdX = rightC - i5[3].width / 2;
		if (rdX + i5[3].width > w - borderW)
		{
			rdX = w - borderW - i5[3].width;
		}
		var rdY = bottomY;

		return Result(
				arrayOf(
					R(i5[0], bigX, bigY),
					R(i5[1], luX, luY),
					R(i5[2], ruX, ruY),
					R(i5[3], rdX, rdY),
					R(i5[4], ldX, ldY)));
	}

	fun R(input : Input, x : Double, y : Double) : ResultItem
	{
		return ResultItem(input, x.toInt(), y.toInt())
	}



	class Input(
			val text : String,
	        val width : Int,
	        val size : FontSize)
	{
		companion object {
			val EMPTY = Input("", 0, FontSize.Small);
		}
		fun deepCopy() = Input(text, width, size);

		override fun toString() = "$text w$width size:$size";
	}

	class Rate(
			val borderRate : Double,
	        val smallCharRate : Double,
	        val bigCharSize : Double,
	        val smallCharSize: Double)

	class Result(
			val items : Array<ResultItem>
	)

	class ResultItem(
		val input : Input,
		val x : Int,
		val y : Int)
	{
		override fun toString() = "$input ($x,$y)";
	}
}

enum class FontSize { Big, Small, SuperSmall };