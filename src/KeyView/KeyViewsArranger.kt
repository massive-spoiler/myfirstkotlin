package wthumbs

object KeyViewsArranger
{
	class Result(
			val xs : Array<Int>,
	        val ys : Array<Int>)

	fun getDiv(width : Int, height : Int,tateBunkatu : Int, yokoBunkatu : Int) : Result
	{
		var yokoB = yokoBunkatu.toDouble();
		var xs =  (0 until yokoBunkatu)
				.map{ width / yokoB * it }
				.map{ it.toInt() }.toTypedArray()

		var tateB = tateBunkatu.toDouble();
		var ys = (0 until tateBunkatu)
				.map{ height / tateB * it }
				.map{ it.toInt() }.toTypedArray();

		return Result(xs, ys);
	}
}