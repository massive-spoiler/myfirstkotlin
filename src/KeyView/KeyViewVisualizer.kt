package wthumbs

import wthumbs.SideBtn as S
import wthumbs.QuadBtn as Q

class KeyViewVisualizer(
	val map : KeyMap,
	val strGetter : SpecialStrGetter,
	val getSmallTextWidth : (String)->Int,
	val getBigTextWidth : (String)->Int,
	val getSuperSmallTextWidth : (String)-> Int,
	val maxBtnTextWidth : Int)
{

	fun huga() = object{ var x = 0; var y = 0; }

	fun getDefault(state : KeyState) : KeyViewInputs
	{
		return visualize(Modifier.C, null, false, null, state);
	}

	fun visualize(
			mod : Modifier, firstN : Btn?, firstEnable : Boolean,
			secondN : Btn?, state : KeyState) : KeyViewInputs
	{
		if (state.selected)
		{
			return getInputs(map.selected, firstN, firstEnable, secondN, state);
		}
		else if (mod == Modifier.C)
		{
			return getInputs(map.main, firstN, firstEnable, secondN, state);
		}
		else if (mod == Modifier.L)
		{
			return getInputs(map.l, firstN,firstEnable, secondN, state);
		}
		else if (mod == Modifier.R)
		{
			return getInputs(map.r, firstN, firstEnable, secondN, state);
		}
		else throw IllegalArgumentException();
	}

	fun getInputs(field : KeyField, firstN : Btn?, firstEnable : Boolean,
		secondN : Btn?, state : KeyState) : KeyViewInputs
	{
		if (firstN == null)
		{
			return KeyViewDefaultVisualizerTemp(this, field, state).getDefault();
		}
		else
		{
			return getExpanded(field, firstN, firstEnable, secondN, state);
		}
	}

	fun getExpanded(field : KeyField, btn : Btn, firstEnable : Boolean,
	                secondBtnN : Btn?, state : KeyState) : KeyViewInputs
	{
		var quintet = field[btn.side][btn.quad];
		var r = KeyViewInputs.createEmpty16();

		var text = strGetter.get(quintet.c, true, state);
		r[btn.side, btn.quad] = KeyViewInput.getCentered(
				text, getBigTextWidth(text), firstEnable);

		var opSide = if(btn.side == S.L) S.R else S.L;
		val get = { quad : Q ->
			var item = quintet[quad];
			var str = strGetter.get(item, true, state);
			KeyViewInput.getCentered(str, getBigTextWidth(str), false);
		};
		r[opSide, Q.L] = get(Q.L);
		r[opSide, Q.U] = get(Q.U);
		r[opSide, Q.R] = get(Q.R);
		r[opSide, Q.D] = get(Q.D);
		r[opSide, Q.S] = get(Q.S);
		r[opSide, Q.X] = get(Q.X);
		//上の候補ビューは強制表示することにする
		var def = KeyViewDefaultVisualizerTemp(this, field, state);
		r[S.L, Q.Y] = def.C(S.L, Q.Y);
		r[S.L, Q.Z] = def.C(S.L, Q.Z);
		r[S.R, Q.Y] = def.C(S.R, Q.Y);
		r[S.R, Q.Z] = def.C(S.R, Q.Z);
		if (firstEnable)
		{
			r[btn.side, btn.quad].pushing = true;
		}

		if (secondBtnN != null)
		{
			r[secondBtnN.side, secondBtnN.quad].pushing = true;
		}
		return r;
	}

	fun toInput(item : KeyMapItem, state:KeyState, center:Boolean, smallMode:Boolean) : KeyViewArranger.Input
	{
		var str = strGetter.get(item, center, state);

		if (center)
		{
			var r = getCenterWidth(str, smallMode);
			return KeyViewArranger.Input(str, r.width, r.fontSize);
		}
		else
		{
			if (smallMode)
			{
				return KeyViewArranger.Input.EMPTY;
			}
			else
			{
				var width = getSmallTextWidth(str);
				return KeyViewArranger.Input(str, width, FontSize.Small);
			}
		}
	}

	companion object{
		//val EmptyInputs =
	}

	class R(val fontSize : FontSize, val width : Int)

	fun getCenterWidth(str : String, smallMode : Boolean) : R
	{
		var width : Int;
		if (smallMode == false)
		{
			width = getBigTextWidth(str);
			if (width < maxBtnTextWidth) return R(FontSize.Big, width);
		}
		width = getSmallTextWidth(str);
		if (width < maxBtnTextWidth) return R(FontSize.Small, width);
		width = getSuperSmallTextWidth(str);
		return R(FontSize.SuperSmall, width);
	}
}