package wthumbs

object KeyAnalyzer
{
	val LL = B(SideBtn.L, QuadBtn.L);
	val LU = B(SideBtn.L, QuadBtn.U);
	val LD = B(SideBtn.L, QuadBtn.D);
	val LR = B(SideBtn.L, QuadBtn.R);
	val LS = B(SideBtn.L, QuadBtn.S);
	val LX = B(SideBtn.L, QuadBtn.X);
	val LY = B(SideBtn.L, QuadBtn.Y);
	val LZ = B(SideBtn.L, QuadBtn.Z);

	val RL = B(SideBtn.R, QuadBtn.L);
	val RU = B(SideBtn.R, QuadBtn.U);
	val RD = B(SideBtn.R, QuadBtn.D);
	val RR = B(SideBtn.R, QuadBtn.R);
	val RS = B(SideBtn.R, QuadBtn.S);
	val RX = B(SideBtn.R, QuadBtn.X);
	val RY = B(SideBtn.R, QuadBtn.Y);
	val RZ = B(SideBtn.R, QuadBtn.Z);

	fun getBtnN(c : Char) : Btn?
	{
		return when(c)
		{
			'E'-> LL;
			'R'-> LU;
			'D'-> LD;
			'F'-> LR;
			'C'-> LS;
			'V'-> LX;
			'Q',
			'A'-> LY;
			'S',
			'W'-> LZ;
			'U'-> RL;
			'I'-> RU;
			'J'-> RD;
			'K'-> RR;
		//'N':
			'M'-> RS;
			','-> RX;
			'L',
			'O'-> RY;
			';',
			'P'-> RZ;
			else -> null;
		}
	}

	fun B(side : SideBtn, quad:QuadBtn) : Btn
	{
		return Btn(side, quad);
	}
}