package wthumbs

import wthumbs.SideBtn as S
import wthumbs.QuadBtn as Q

class KeyViewDefaultVisualizerTemp(
	val visualizer : KeyViewVisualizer,
	val field : KeyField,
	val state : KeyState)
{
	fun getDefault() : KeyViewInputs
	{
		return KeyViewInputs(
				arrayOf(
						C(S.L, Q.Y), C(S.L, Q.Z), C(S.R, Q.Y), C(S.R, Q.Z),
						G(S.L, Q.L), G(S.L, Q.U), G(S.R, Q.L), G(S.R, Q.U),
						G(S.L, Q.D), G(S.L, Q.R), G(S.R, Q.D), G(S.R, Q.R),
						G(S.L, Q.S), G(S.L, Q.X), G(S.R, Q.S), G(S.R, Q.X)))
	}

	fun C(side : S, quad : Q) = G(side, quad, true);

	fun G(side : S, quad : Q, smallMode : Boolean = false) : KeyViewInput
	{
		return getDefaultKey(field[side][quad], smallMode);
	}

	fun  getDefaultKey(key : KeyQuintet, smallMode : Boolean) : KeyViewInput {
		return KeyViewInput(
				arrayOf(get(key.c, smallMode, true),
						get(key.l, smallMode),
						get(key.u, smallMode),
						get(key.r, smallMode),
						get(key.d, smallMode)), false);
	}

	fun get(item : KeyMapItem, smallMode : Boolean, center : Boolean = false) : KeyViewArranger.Input
	{
		return visualizer.toInput(item, state, center, smallMode);
	}

	companion object {
		fun getIndex(side : S, quad : Q) : Int
		{
			if (side == S.L) {
				return when (quad)
				{
					Q.Y-> 0;
					Q.Z-> 1;
					Q.L-> 4;
					Q.U-> 5;
					Q.D-> 8;
					Q.R-> 9;
					Q.S-> 12;
					Q.X-> 13;
				}
			} else if (side == S.R) {
				return when (quad)
				{
					Q.Y-> 2;
					Q.Z-> 3;
					Q.L-> 6;
					Q.U-> 7;
					Q.D-> 10;
					Q.R-> 11;
					Q.S-> 14;
					Q.X-> 15;
				}
			}
			throw IllegalArgumentException();
		}
	}
}