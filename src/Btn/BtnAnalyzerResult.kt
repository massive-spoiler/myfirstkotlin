package wthumbs

class BtnAnalyzerResult(
	val runN : KeyMapItem?,
	val firstBtnN : Btn?,
	val secondBtnN : Btn?,
	val firstEnable : Boolean,
	val mod : Modifier)
