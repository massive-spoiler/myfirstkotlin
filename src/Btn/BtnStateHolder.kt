package wthumbs

class BtnStateHolder
{
	val quads = Array(KeyField.length, { QuadState() });

	operator fun get(btn : SideBtn) : QuadState = quads[btn.ordinal]

	fun push(btn : Btn)
	{
		this[btn.side][btn.quad] = true;
	}

	fun isPushed(btn : Btn) : Boolean
	{
		return this[btn.side][btn.quad];
	}

	fun release(btn : Btn)
	{
		this[btn.side][btn.quad] = false;
	}

	/// <summary>
	/// 押されているボタンを数える
	/// </summary>
	fun countPushed() : Int
	{
		var sum = 0;
		for(quad in quads)
		{
			for (b in quad.bits)
			{
				if (b) ++sum;
			}
		}
		return sum;
	}
}

class QuadState
{
	val bits = Array<Boolean>(KeyQuad.length){ false }

	operator fun get(quad : QuadBtn) = bits[quad.ordinal]
	operator fun set(quad : QuadBtn, value : Boolean){ bits[quad.ordinal] = value }
}