package wthumbs

object BtnRunner
{
	fun RunN(map : KeyMap, selected : Boolean, modifier : Modifier, first : Btn, secondN : Btn?) : KeyMapItem?
	{
		var field : KeyField?
		if (selected)
		{
			field = map.selected;
		}
		else if (modifier == Modifier.C)
		{
			field = map.main;
		}
		else if (modifier == Modifier.L)
		{
			field = map.l;
		}
		else if (modifier == Modifier.R)
		{
			field = map.r;
		}
		else throw IllegalArgumentException();

		if (secondN == null)
		{
			return field[first.side][first.quad].c;
		}

		val second = secondN;
		if (first.side == second.side) return null;

		if(KeyQuintet.isValidQuad(second.quad)) {
			return field[first.side][first.quad][second.quad];
		}
		else return null
	}
}