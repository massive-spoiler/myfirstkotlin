package wthumbs

class BtnAnalyzer(val map : KeyMap)
{
	var firstBtnN : Btn? = null
	var mod : Modifier = Modifier.C
	var doublePushed = false
	var caps = false
	val holder = BtnStateHolder();

	fun Push(btn : Btn, state : KeyState) : BtnAnalyzerResult
	{
		holder.push(btn);

		if (firstBtnN == null)
		{
			firstBtnN = btn;
		}
		else if (holder.countPushed() == 2)
		{
			//2つ目を押した時に実行(3つめでは実行しない)
			var result = BtnRunner.RunN(map, state.selected, mod, firstBtnN!!, btn);
			if (result != null)
			{
				doublePushed = true;
				return Result(btn, result);
			}
		}
		return Result();
	}

	fun Release(btn : Btn, state : KeyState) : BtnAnalyzerResult
	{
		if (holder.isPushed(btn) == false || firstBtnN == null) return Result();
		holder.release(btn);

		if (holder.countPushed() == 0)
		{
			//最初のボタンを離した時に起動
			var first = firstBtnN!!;
			firstBtnN = null;
			if (doublePushed)
			{
				doublePushed = false;
				return Result();
			}
			else
			{
				//二個押しが起動してない場合は、ボタンを離すと一個押しが起動する
				return Result(null, BtnRunner.RunN(map,
						state.selected, mod, first, null));
			}
		}
		else return Result();
	}

	fun Result(secondBtnN : Btn? = null, runN_ : KeyMapItem? = null) : BtnAnalyzerResult
	{
		var runN = runN_
		if (runN != null)
		{
			if (Run(runN))
			{
				runN = null;
			}
		}

		return BtnAnalyzerResult(runN, firstBtnN, secondBtnN, !doublePushed, mod);
	}

	/// 自分でできるものについては自分でやる
	fun Run(run : KeyMapItem) : Boolean
	{
		return when (run.special) {
			SpecialKey.C -> {
				mod = Modifier.C; true;
			}
			SpecialKey.R -> {
				mod = Modifier.R; true;
			}
			SpecialKey.L -> {
				mod = Modifier.L; true;
			}
			SpecialKey.Caps -> {
				caps = !caps; true;
			}
			else -> false;
		}
	}
}