package wthumbs

import java.nio.file.Files
import java.nio.file.Paths

object Initializer
{
	class Result(
		val btnAnalyzer : BtnAnalyzer,
		val candidateDict : CandidateDict,
		val visualizer : KeyViewVisualizer)


	fun Initialize(
		getSmallTextWidth : (String)->Int, getBigTextWidth : (String) -> Int,
		getSuperSmallTextWidth:(String)->Int, maxBtnTextWidth : Int) : Result
	{
		var lines = Files.readAllLines(Paths.get("res/KeySetting.txt"), Charsets.UTF_8);
		var map = KeymapScriptParser.Parse(lines.toTypedArray());
		lines = Files.readAllLines(Paths.get("res/itaijidata.txt"), Charsets.UTF_8);
		var itaiji = Itaiji(lines.toTypedArray());
		var strGetter = SpecialStrGetter(itaiji);
		var visualizer = KeyViewVisualizer(map, strGetter, getSmallTextWidth, getBigTextWidth, getSuperSmallTextWidth, maxBtnTextWidth);
		var btnAnalyzer = BtnAnalyzer(map);
		//var myDictionary = MyDictionary();
		var candidateDict = CandidateDictCreator.Do(itaiji, Langu.En)//, myDictionary);

		return Result(btnAnalyzer, candidateDict, visualizer);
	}
}