package wthumbs

class KeyMapItem private constructor(
		val special : SpecialKey = SpecialKey.NotSpecial,
		val char : Char = 0.toChar())
{
	val isSpecial : Boolean get() = special != SpecialKey.NotSpecial

	constructor(str : String) : this(getSpecial(str)){}
	constructor(c : Char) : this(char = c){}

	override fun toString() : String
	{
		return  if(isSpecial){ "[" + special.name + "]"} else{ char.toString() };
	}

	companion object{
		fun getSpecial(str : String) : SpecialKey{
			try {
				return SpecialKey.valueOf(str)
			}
			catch(e : Exception)
			{
				throw KeyMapException("$str というSpecialKeyはありません", null);
			}
		}
	}
}

