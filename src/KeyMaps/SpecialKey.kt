package wthumbs

enum class SpecialKey
{
	NotSpecial, Space, Del,
	a_A, Caps, Upper, Left,Up,Right,Down,
	Enter, S1,S2,S3,S4, Dict, S1N,S2N,S3N,S4N,
	SLeft,SUp,SRight,SDown,
	Paste,Settings,Cut,Copy,Search,Deselect,
	Unused, Undo, Redo,
	Home,Top,End,Bottom,
	SHome,STop,SEnd,SBottom,
	SelectAll, CutAll, SelectWord, CutWord, SelectLine, CutLine,
	PgUp,SPgUp,PgDn,SPgDn, WLeft, WRight, WSLeft, WSRight,
	i1,i2,i3,i4,i5,i6,i7,i8,i9,OK,Cancel, Unicode,
	R,C,L
}