package wthumbs

class KeyMapScriptParserHelperState
{
	var modifier: Int = -1;
	var side: Int = -1;
	var position: Int = -1;
}

class KeyMapScriptParserHelper
{
	val map = KeyMap();
	val state = KeyMapScriptParserHelperState();
	/// <summary>
	/// *CLRみたいなルールに従った4文字を渡す。
	/// エラーならKeyMapExceptionが飛ぶ。
	/// </summary>
	fun SetState(m4 : String)
	{
		if (m4.length != 4) throw KeyMapException();
		state.modifier = KeyMap.getIndex(m4[1]);
		state.side = KeyField.getIndex(m4[2]);
		state.position = KeyQuad.getIndex(m4[3]);
	}
	fun Analyze(line : String)
	{
		var result = KeyMapScriptParserAnalyzer().Do(line);
		SetItems(result);
	}
	fun SetItems(i5 : KeyQuintet)
	{
		try
		{
			var a = map.array[state.modifier];
			var b = a.array[state.side];
			var c = b.array[state.position];
			if (c == null)
			{
				b.array[state.position] = i5;
			}
			else throw KeyMapException("同じポジションに設定しようとしています", null);
		}
		catch (e : Exception)
		{
			throw KeyMapException("ポジションが設定されていません", e);
		}
	}
}