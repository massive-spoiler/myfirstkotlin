package wthumbs

import java.util.HashMap

class Itaiji(lines : Array<String>)
{
	val dict = HashMap<Char, String>();
	val comparingDict = HashMap<Char, Char>();

	init {
		for (line in lines)
		{
			val start = line[0];
			dict.put(start, line.substring(1));
			val startL = Character.toLowerCase(start);
			for (k in 1 until line.length)
			{
				comparingDict.put(line[k], startL);
			}
		}
	}

	fun get(baseChar : Char, itaijiNum : Int) : Char?
	{
		val line = dict.get(baseChar)
		if (line != null && itaijiNum < line.length)
		{
			return line[itaijiNum];
		}
		else return null;
	}

	fun comparisonChars(str : String) : CharArray
	{
		var result = CharArray(str.length)
		//var result = Array<Char>(str.length, {0.toChar()});
		for (i in result.indices)
		{
			var c = str[i];
			val converted = comparingDict.get(c)
			if (converted != null)
			{
				result[i] = Character.toLowerCase(converted);
			}
			else
			{
				result[i] = Character.toLowerCase(c);
			}
		}
		return result;
	}

	fun comparisonStr(str : String) : String
	{
		return String(comparisonChars(str))
	}
}