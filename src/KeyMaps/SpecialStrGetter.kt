package wthumbs

class SpecialStrGetter(val itaiji : Itaiji)
{
	fun get(item : KeyMapItem, isBig : Boolean, state : KeyState) : String
	{
		if (item.isSpecial)
		{
			return getSpecial(item.special, isBig, state);
		}
		else
		{
			return getChar(item.char, state).toString();
		}
	}

	fun getItaijiN(special : SpecialKey, cursorChar : Char?) : Char?
	{
		if (SpecialKey.i1 <= special && special <= SpecialKey.i9)
		{
			if (cursorChar != null)
			{
				return itaiji.get(cursorChar, special.ordinal - SpecialKey.i1.ordinal);
			}
		}
		return null;
	}

	fun getSpecial(special : SpecialKey, isBig : Boolean, s : KeyState) : String
	{
		if (SpecialKey.i1 <= special && special <= SpecialKey.i9)
		{
			var itaiji = getItaijiN(special, s.cursorChar);
			if(itaiji != null)
			{
				return itaiji.toString();
			}
			else return "";
		}
		return when (special)
		{
			SpecialKey.R -> "$&=";
			SpecialKey.C -> if(s.caps) "ABC" else "abc";
			SpecialKey.a_A -> "a/A";
			SpecialKey.Caps -> if(s.caps) "Caps" else "Caps";
			SpecialKey.Upper -> "Upper";
			SpecialKey.Bottom -> if(isBig) "Bottom" else "⤓";
			SpecialKey.STop -> if(isBig)"SelTop" else "⍐";
			 SpecialKey.SBottom -> if(isBig) "SelBottom" else "⍗";
			 SpecialKey.Top -> if(isBig) "Top" else "⤒";
			 SpecialKey.Home-> if(isBig) "Home" else "⇤";
			 SpecialKey.End-> if(isBig) "End" else "⇥";
			 SpecialKey.SHome-> if(isBig) "SelHome" else "⍇";
			 SpecialKey.SEnd-> if(isBig) "SelEnd" else "⍈";
			 SpecialKey.Up-> if(isBig) "Up" else "↑";
			 SpecialKey.Down-> if(isBig) "Down" else "↓";
			 SpecialKey.Left-> if(isBig) "Left" else "←";
			 SpecialKey.Right-> if(isBig) "Right" else "→";
			 SpecialKey.SUp-> if(isBig) "SelUp" else "⇑";
			 SpecialKey.SLeft-> if(isBig) "SelLeft" else "⇐";
			 SpecialKey.SDown-> if(isBig) "SelDown" else "⇓";
			 SpecialKey.SRight-> if(isBig) "SelRight" else "⇒";
			 SpecialKey.SelectAll-> if(isBig) "SelAll" else "All";
			 SpecialKey.CutAll-> if(isBig) "CutAll" else "✂All";
			 SpecialKey.SelectLine-> if(isBig) "SelLine" else "Line";
			 SpecialKey.CutLine-> if(isBig) "SelLine" else "✂Line";
			 SpecialKey.SelectWord-> if(isBig) "SelWord" else "Word";
			 SpecialKey.CutWord-> if(isBig) "CutWord" else "✂Word";
			 SpecialKey.Settings-> if(isBig) "Settings" else "⚙";
			 SpecialKey.Unused-> "";
			 SpecialKey.S1-> getSuggestion(s, isBig, 0);
			 SpecialKey.S2-> getSuggestion(s, isBig, 1);
			 SpecialKey.S3-> getSuggestion(s, isBig, 2);
			 SpecialKey.S4-> getSuggestion(s, isBig, 3);
			 SpecialKey.S1N, SpecialKey.S2N, SpecialKey.S3N, SpecialKey.S4N ->
				 if(isBig) "<" else "";
			SpecialKey.Del -> "⌫";
			SpecialKey.Enter -> "⏎";
			SpecialKey.Copy -> "Copy";
			SpecialKey.Cut -> "Cut";
			SpecialKey.Paste -> "Paste";
			SpecialKey.Search -> if(isBig) "Search" else "🔍";
			SpecialKey.Undo -> if(s.undoable) (if(isBig) "Undo" else "↩") else "";
			SpecialKey.Redo -> if(s.redoable) (if(isBig) "Redo" else  "↪") else "";
			else -> special.toString()
		}
	}

	fun getSuggestion(s : KeyState, isBig : Boolean, x : Int) : String
	{
		if (x < s.candidates.size)
		{
			return if(isBig) s.candidates[x] else "Sug" + (x + 1);
		}
		else return "";
	}

	fun getChar(c : Char, s : KeyState) : Char
	{
		if (s.caps)
		{
			return Character.toUpperCase(c);
		}
		else return c;
	}
}