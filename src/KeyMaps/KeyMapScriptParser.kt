package wthumbs

class KeymapScriptParser
{
    val helper = KeyMapScriptParserHelper()

    companion object {
        fun Parse(script : Array<String>) : KeyMap
        {
            return KeymapScriptParser().Do(script);
        }
    }
    fun Do(script : Array<String>) : KeyMap {
        for (i in script.indices) {
            try {
                var line = script[i];
                line = RemoveComment(line).trim()
                if (line.length <= 1) continue;
                if (line[0] == '*' && line.length == 4) {
                    helper.SetState(line);
                } else {
                    helper.Analyze(line);
                }
            } catch (e: KeyMapException) {
                var ne = KeyMapException("${i + 1}行目でエラーです", e);
                ne.Line = i + 1;
                throw ne;
            }
        }
        if (nullCheck(helper.map)) {
            return helper.map;
        } else throw KeyMapException("配置されていないものがあります");
    }

    fun nullCheck(map : KeyMap) : Boolean
    {
        for(a in map.array)
        {
            for(b in a.array)
            {
                for(c in b.array)
                {
                    if(c == null) return false;
                    for (d in c.array)
                    {
                        if(d == null) return false;
                    }
                }
            }
        }
        return true;
    }

    fun RemoveComment(s : String) : String
    {
        var ind = s.indexOf("//");
        if (ind >= 0)
        {
            return s.substring(0, ind);
        }
        else return s;
    }
}
