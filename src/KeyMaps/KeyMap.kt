package wthumbs

class KeyQuintet
{
	companion object {
		val length = 7;
		fun isValidQuad(btn : QuadBtn) : Boolean
		{
			return QuadBtn.L <= btn && btn <= QuadBtn.X;
		}
	}
	val array = Array<KeyMapItem?>(length, {null})
	val c : KeyMapItem get() = array[0]!!
	val l : KeyMapItem get() = array[1]!!
	val u : KeyMapItem get() = array[2]!!
	val d : KeyMapItem get() = array[3]!!
	val r : KeyMapItem get() = array[4]!!
	val s : KeyMapItem get() = array[5]!!
	val x : KeyMapItem get() = array[6]!!

	operator fun get(btn : QuadBtn) : KeyMapItem = array[btn.ordinal + 1]!!
	override fun toString() = array.joinToString();
}

enum class QuadBtn { L, U, D, R, S, X, Y, Z }

class KeyQuad {
	val array = Array<KeyQuintet?>(length, { null })

	var l: KeyQuintet
		get() = array[0]!!
		set(value) { array[0] = value }

	var u: KeyQuintet
		get() = array[1]!!
		set(value) { array[1] = value }
	var d: KeyQuintet
		get() = array[2]!!
		set(value) { array[2] = value }
	var r: KeyQuintet
		get() = array[3]!!
		set(value) { array[3] = value }
	var s: KeyQuintet
		get() = array[4]!!
		set(value) { array[4] = value }
	var x: KeyQuintet
		get() = array[5]!!
		set(value) { array[5] = value }
	var y: KeyQuintet
		get() = array[6]!!
		set(value) { array[6] = value }
	var z: KeyQuintet
		get() = array[7]!!
		set(value) { array[7] = value }

	operator fun get(btn: QuadBtn) = array[btn.ordinal]!!

	override fun toString() = "L/$l U/$u D/$d R/$r S/$s X/$x Y/$y Z/$z";

	companion object {
		val length = 8;
		fun getIndex(pos: Char) : Int {
			return when (pos) {
				'L' -> 0
				'U' -> 1
				'D' -> 2
				'R' -> 3
				'S' -> 4
				'X' -> 5;
				'Y' -> 6;
				'Z' -> 7;
				else -> throw KeyMapException();
			}
		}
	}
}

class KeyField
{
	val array = Array<KeyQuad>(length, { KeyQuad()});
	var l : KeyQuad
		get() = array[0]
		set(value) { array[0] = value }
	var r : KeyQuad
		get() = array[0]
		set(value) { array[0] = value }

	companion object
	{
		val length = 2
		fun getIndex(pos:Char) =
		when (pos)
		{
			'L' ->  0;
			'R' -> 1;
			else -> throw KeyMapException();
		}
	}

	operator fun get(side : SideBtn) : KeyQuad = array[side.ordinal]

	override fun toString() = "$l $r"
}

enum class SideBtn { L,R }

fun SideBtn.Opposite(s : SideBtn) : SideBtn = if(s == SideBtn.L){ SideBtn.R }else{ SideBtn.L}

class KeyMap {
	val array = Array(length){ KeyField() }

	var main: KeyField
		get() = array[0]
		set(value) { array[0] = value }

	var l: KeyField
		get() = array[1]
		set(value) { array[1] = value }

	var r: KeyField
		get() = array[2]
		set(value) { array[2] = value }

	var selected: KeyField
		get() = array[3]
		set(value) { array[3] = value }

	operator fun get(side: SideBtn) = array[side.ordinal + 1]

	companion object {
		val length = 4
		fun getIndex(pos: Char): Int {
			return when (pos) {
				'C' -> 0;
				'L' -> 1;
				'R' -> 2;
				'S' -> 3;
				else -> throw KeyMapException();
			}
		}
	}
}
enum class Modifier {
	C, L, R
}