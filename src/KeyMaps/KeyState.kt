package wthumbs

class KeyState(
	val cursorChar : Char?,
	val candidates : Array<String>,
	val caps : Boolean,
	val selected : Boolean,
	val undoable : Boolean,
	val redoable : Boolean)
{
	companion object {
		fun getDefault() = KeyState(null, arrayOf<String>(), false, false, false, false);
	}
}