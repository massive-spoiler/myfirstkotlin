package wthumbs

class KeyMapScriptParserAnalyzer()
{
	class IndexString(val line : String, var index : Int)
	{
		operator fun invoke(i : Int) : Char? {
			val dest = index + i
			if(dest < line.length) {
				return line[index + i]
			}
			else return null
		}
		//バッキングフィールドは必要なければ生成されないらしいがthis property has a backing field...
		val length : Int get() = line.length
	}

	private val result = KeyQuintet();
	private var pos = 0;

	fun Do(_line : String) : KeyQuintet
	{
		val line = IndexString(_line, 0)
		while (line.index < line.length)
		{
			Analyze(line);
		}

		for (item in result.array)
		{
			if (item == null) throw KeyMapException("文字かSpecialが7個必要です", null);
		}

		return result;
	}

	/// <summary>
	/// [hoge] これはspecial
	/// [[ 2連続の場合は (char)[
	/// </summary>
	fun Analyze(line : IndexString) {
		val c = line(0)
		if (c == '[') {
			val c2 = line(1);
			if (c2 == '[') {
				addChar('[');
				line.index += 2;
			} else {
				line.index += 1;
				addSpecial(line);
			}
		} else if (c != null) {
			addChar(c);
			line.index += 1
		}
		else throw KeyMapException("")
	}

	fun addChar(c : Char)
	{
		result.array[pos] = KeyMapItem(c);
		++pos;
	}

	fun addSpecial(_line : IndexString)
	{
		val i = _line.index
		val line = _line.line

		var tojiru = line.indexOf(']', i);
		if (tojiru >= 0)
		{
			var special = line.substring(i, tojiru);
			result.array[pos] = KeyMapItem(special);
			++pos;
			_line.index = tojiru + 1;
		}
		else throw KeyMapException("カッコ閉じる']'が見つかりません", null);
	}
}