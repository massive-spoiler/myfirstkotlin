package wthumbs

import java.awt.event.KeyEvent
import javax.swing.JTextArea

class EditBoxFunction(
	val tb : JTextArea,
	val analyzer : BtnAnalyzer,
	val visualizer : KeyViewVisualizer,
	val candidateDict : CandidateDict,
	val visualize_Form : (KeyViewInputs)->Unit)
{
	val spExec = SpecialExecuter(tb)
	var candidates = arrayOf<String>();

	fun onKeyDown(e : KeyEvent)
	{
		var btn = getBtnN(e);
		if (btn != null)
		{
			var r = Push(btn);
			if (r.runN != null)
			{
				input(r.runN!!);
			}
			setCandidates();
			visualize(r);
		}
		e.consume()
	}

	fun onKeyUp(e : KeyEvent)
	{
		var btn = getBtnN(e);
		if (btn != null)
		{
			var r = release(btn);
			if (r.runN != null)
			{
				input(r.runN!!);
			}
			setCandidates();
			visualize(r);
		}

		e.consume();
	}

	fun putChar(c : Char)
	{
		spExec.selectedText = c.toString();
	}

	fun putStr(s : String)
	{
		spExec.selectedText = s;
	}

	fun input(item : KeyMapItem)
	{
		if (item.isSpecial == false)
		{
			putChar(visualizer.strGetter.getChar(item.char, getState()));
		}
		else if (SpecialKey.i1 <= item.special && item.special <= SpecialKey.i9)
		{
			var c = visualizer.strGetter.getItaijiN(item.special, spExec.getCursorChar());
			if (c != null) putChar(c);
		}
		else
		{
			spExec.doSpecial(item.special, candidates);
		}
	}

	fun getBtnN(e : KeyEvent) : Btn?
	{
		//Console.WriteLine(key);
		return when (e.keyCode)
		{
			KeyEvent.VK_E -> KeyAnalyzer.getBtnN('E');
			KeyEvent.VK_R->  KeyAnalyzer.getBtnN('R');
			KeyEvent.VK_T->  KeyAnalyzer.getBtnN('T');
			KeyEvent.VK_D->  KeyAnalyzer.getBtnN('D');
			KeyEvent.VK_F->  KeyAnalyzer.getBtnN('F');
			KeyEvent.VK_G->  KeyAnalyzer.getBtnN('G');
			KeyEvent.VK_C->  KeyAnalyzer.getBtnN('C');
			KeyEvent.VK_V->  KeyAnalyzer.getBtnN('V');
			KeyEvent.VK_B->  KeyAnalyzer.getBtnN('B');
			KeyEvent.VK_S->  KeyAnalyzer.getBtnN('S');
			KeyEvent.VK_A->  KeyAnalyzer.getBtnN('A');
			KeyEvent.VK_Q->  KeyAnalyzer.getBtnN('Q');
			KeyEvent.VK_W->  KeyAnalyzer.getBtnN('W');
			KeyEvent.VK_Y->  KeyAnalyzer.getBtnN('Y');
			KeyEvent.VK_U->  KeyAnalyzer.getBtnN('U');
			KeyEvent.VK_I->  KeyAnalyzer.getBtnN('I');
			KeyEvent.VK_H->  KeyAnalyzer.getBtnN('H');
			KeyEvent.VK_J->  KeyAnalyzer.getBtnN('J');
			KeyEvent.VK_K->  KeyAnalyzer.getBtnN('K');
			KeyEvent.VK_N->  KeyAnalyzer.getBtnN('N');
			KeyEvent.VK_M->  KeyAnalyzer.getBtnN('M');
			KeyEvent.VK_COMMA-> KeyAnalyzer.getBtnN(',');
			KeyEvent.VK_O-> KeyAnalyzer.getBtnN('O');
			KeyEvent.VK_L-> KeyAnalyzer.getBtnN('L');
			KeyEvent.VK_P-> KeyAnalyzer.getBtnN('P');
			KeyEvent.VK_SHIFT-> KeyAnalyzer.getBtnN(';');
			else -> null;
		}
	}

	fun Push(btn:Btn) : BtnAnalyzerResult
	{
		return analyzer.Push(btn, getState());
	}

	fun release(btn:Btn) : BtnAnalyzerResult
	{
		return analyzer.Release(btn, getState());
	}

	fun getState() : KeyState
	{
		return KeyState(spExec.getCursorChar(), candidates, analyzer.caps, spExec.isSelected, false, false);
	}

	fun visualize(r : BtnAnalyzerResult)
	{
		var visual = visualizer.visualize(r.mod, r.firstBtnN, r.firstEnable, r.secondBtnN, getState());
		visualize_Form(visual);
	}

	fun setCandidates()
	{
		if (spExec.isSelected == false)
		{
			var r = WordGetter.getWordN(tb.text, tb.selectionStart);
			if (r != null)
			{
				candidates = candidateDict.getWordList(
						tb.text.substring(r.index, r.index + r.length));
				//DrawCandidates(Candidates);
				return;
			}
		}
		candidates = arrayOf<String>();
		//DrawCandidates(Candidates);
	}


}