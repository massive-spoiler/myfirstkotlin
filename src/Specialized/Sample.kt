package wthumbs
import java.awt.*
import java.awt.event.*
import java.io.*
import java.nio.file.Files
import java.nio.file.Paths
import javax.swing.*
class Sample:JFrame(), KeyListener {
    val tb : JTextArea
	val fonts : BigAndSmallFonts
	val funcs : EditBoxFunction
	val kv16 : KeyView16Ctl
    init{
        this.setSize(400, 600)
        this.defaultCloseOperation = JFrame.EXIT_ON_CLOSE
        tb = JTextArea()
        val scroll = JScrollPane(tb)
	    tb.addKeyListener(this)
        this.add(scroll, BorderLayout.CENTER)

	    fonts = BigAndSmallFonts(
			    Font(Font.SANS_SERIF, Font.BOLD, BigAndSmallFonts.bigCharSize.toInt()),
			    Font(Font.SANS_SERIF, Font.BOLD, BigAndSmallFonts.smallCharSize.toInt()),
			    Font(Font.SANS_SERIF, Font.BOLD, BigAndSmallFonts.superSmallCharSize.toInt()))
	    var ini = Initializer.Initialize(
			    { getSmallTextWidth(it) },
			    { getBigTextWidth(it) },
			    { getSuperSmallTextWidth(it) }, 98)
		funcs = EditBoxFunction(tb, ini.btnAnalyzer, ini.visualizer, ini.candidateDict,
				{ visualize_form(it) })

	    kv16 = KeyView16Ctl({ fonts })

	    kv16.preferredSize = Dimension(400,400)
	    this.add(kv16, BorderLayout.SOUTH)
    }

	override fun keyReleased(p0: KeyEvent?) {
		if(p0 != null)
			funcs.onKeyUp(p0)
	}

	override fun keyTyped(p0: KeyEvent?) {
		if(p0 != null)
			p0.consume()
	}

	override fun keyPressed(p0: KeyEvent?) {
		if(p0 != null)
			funcs.onKeyDown(p0)
	}

	fun visualize_form(inputs : KeyViewInputs)
	{
		kv16.set(inputs)
	}

	fun getSuperSmallTextWidth(text : String) : Int
	{
		val metrics = getFontMetrics(fonts.superSmallFont)
		return metrics.stringWidth(text)
	}
	fun getSmallTextWidth(text : String) : Int
	{
		val metrics = getFontMetrics(fonts.smallFont)
		return metrics.stringWidth(text)
	}
	fun getBigTextWidth(text : String) : Int
	{
		val metrics = getFontMetrics(fonts.bigFont)
		return metrics.stringWidth(text)
	}

	companion object {
        private val serialVersionUID = 1L
        @JvmStatic fun main(args:Array<String>) {
            val sample = Sample()
            sample.setVisible(true)
        }
    }
}