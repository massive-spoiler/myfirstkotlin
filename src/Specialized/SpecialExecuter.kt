package wthumbs

import javax.swing.JTextArea

class SpecialExecuter(
	val tb : JTextArea)
{
	var selectedText : String
		get() = tb.selectedText
		set(value){ tb.replaceRange(value, tb.selectionStart, tb.selectionEnd) }

	var selectionLength : Int
		get() = tb.selectionEnd - tb.selectionStart
		set(value){ tb.selectionEnd = tb.selectionStart + value }

	val isSelected : Boolean get() = selectionLength != 0;

	val selectionStart : Int get() = tb.selectionStart

	fun doSpecial(special : SpecialKey, candidates : Array<String>)
	{
		when (special) {
			SpecialKey.Space -> selectedText = " ";
			SpecialKey.Del -> {
				if (selectionLength == 0) {
					var cursor = selectionStart;
					if (cursor != 0) {
						tb.select(cursor - 1, cursor);
						tb.cut();
					}
				} else {
					selectedText = "";
				}
			}
			SpecialKey.a_A -> {
				var c = getCursorChar();
				if (c != null) {
					if (Character.isLowerCase(c)) {
						c = Character.toUpperCase(c);
					} else if (Character.isUpperCase(c)) {
						c = Character.toLowerCase(c);
					} else return

					tb.select(tb.selectionStart - 1, tb.selectionStart);
					tb.cut();
					selectedText = c.toString();
				}
			}
			SpecialKey.Upper -> {
				if (this.isSelected == false) {
					var r = UpperFunction.DoN(tb.text, tb.selectionStart);
					if (r != null) {
						tb.selectionStart = r.index;
						selectionLength = r.length;
						selectedText = r.word + if(r.appendSpace) " " else "";
						if (r.cursorMove) {
							tb.selectionStart++;
						}
					}
				}
			}
			SpecialKey.Enter -> selectedText = "\n";

			SpecialKey.Left ->
			if (tb.selectionStart != 0) {
				--tb.selectionStart;
			}

			SpecialKey.Right-> ++tb.selectionStart;

			SpecialKey.SLeft->
			if (tb.selectionStart != 0)
			{
				tb.selectionStart--;
				selectionLength++;
			}

			SpecialKey.SRight-> selectionLength++;

			SpecialKey.S1-> DoSuggest(0, candidates);
			SpecialKey.S2-> DoSuggest(1, candidates);
			SpecialKey.S3-> DoSuggest(2, candidates);
			SpecialKey.S4-> DoSuggest(3, candidates);
			SpecialKey.S1N-> DoSuggest(0, candidates, suppressSpace = true);
			SpecialKey.S2N-> DoSuggest(1, candidates, suppressSpace = true);
			SpecialKey.S3N-> DoSuggest(2, candidates, suppressSpace = true);
			SpecialKey.S4N-> DoSuggest(3, candidates, suppressSpace = true);
			else->{}
		}
	}

	fun DoSuggest(index : Int, candidates : Array<String>, append :String = "", suppressSpace : Boolean = false)
	{
		if (index < candidates.size)
		{
			var text = tb.text;
			var r = WordGetter.getWordN(text, tb.selectionStart);
			if (r != null)
			{
				val space= !suppressSpace && r.appendSpace;
				tb.selectionStart = r.index;
				selectionLength = r.length;
				selectedText = candidates[index] + append + if(space)" " else "";
				if (r.cursorMove && !suppressSpace)
				{
					tb.selectionStart++;
				}
			}
		}
	}

	fun getCursorChar() : Char?
	{
		if (tb.selectionStart != 0)
		{
			return tb.getText(tb.selectionStart - 1, 1)[0]
		}
		else return null;
	}

}