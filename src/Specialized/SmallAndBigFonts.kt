package wthumbs

import java.awt.Font

class BigAndSmallFonts(
	val bigFont : Font,
	val smallFont : Font,
	val superSmallFont : Font) {
	companion object {
		val smallCharRate: Double = 0.2;
		val bigCharSize = 14.0;
		val smallCharSize: Double = 12.0;
		val superSmallCharSize: Double = 9.0;
		val borderRate: Double = 0.01;
	}
}