package wthumbs

import java.awt.Color
import java.awt.Dimension
import java.awt.Point
import java.awt.event.ComponentAdapter
import java.awt.event.ComponentEvent
import java.util.*
import javax.swing.JComponent

class KeyView16Ctl(getFonts : ()->BigAndSmallFonts) : JComponent() {
	val kv16 : Array<KeyViewCtl>
	val TateBunkatu = 4;
	val YokoBunkatu = 4;
	val Border = 5;

	init{
		this.background = Color.BLACK
		kv16 = Array(16) { KeyViewCtl(getFonts) }
		for(ctl in kv16)
			this.add(ctl)

		this.addComponentListener(object : ComponentAdapter() {
			override fun componentResized(e : ComponentEvent) = arrangeKv()
		})

	}

	fun set(inputs : KeyViewInputs)
	{
		if (kv16.size != inputs.items.size)
			throw IllegalArgumentException();

		for (i in inputs.items.indices)
		{
			var item = inputs.items[i];
			kv16[i].set(item.i5, item.pushing);
		}
	}

	fun arrangeKv()
	{
		var r = KeyViewsArranger.getDiv(this.width, this.height, TateBunkatu, YokoBunkatu);

		var xs = r.xs;
		var ys = r.ys;
		var queue = ArrayDeque<KeyViewCtl>()
		queue.addAll(kv16)

		for (k in 0 until ys.size)
		{
			for (i in 0 until xs.size)
			{
				var kv = queue.removeFirst()
				var x = xs[i];
				var y = ys[k];
				var width : Int; var height : Int;
				if (i + 1 < xs.size) {
					width = xs[i + 1] - x - Border;
				} else {
					width = this.width - x - Border;
				}
				if (k + 1 < ys.size) {
					height = ys[k + 1] - y - Border;
				} else {
					height = this.height - y - Border;
				}

				kv.location = Point(x, y);
				kv.size = Dimension(width, height);
			}
		}
		this.repaint()
	}
}