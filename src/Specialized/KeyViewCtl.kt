package wthumbs

import java.awt.Color
import java.awt.Font
import java.awt.Graphics
import javax.swing.JComponent

class KeyViewCtl(val getFonts : ()-> BigAndSmallFonts) : JComponent()
{
	var i5 : Array<KeyViewArranger.Input>? = null

	fun set(i5_ : Array<KeyViewArranger.Input>, pushed : Boolean)
	{
		i5 = i5_;
		if (pushed)
		{
			this.background = Color.WHITE;
		}
		else
		{
			this.background = Color.GRAY;
		}
		this.repaint();
	}

	override fun paintComponent(g : Graphics) {
		if (i5 == null) return;
		var r = KeyViewArranger.Arrange(
				KeyViewArranger.Rate(
						BigAndSmallFonts.borderRate,
						BigAndSmallFonts.smallCharRate,
						BigAndSmallFonts.bigCharSize,
						BigAndSmallFonts.smallCharSize),
				i5!!, this.width, this.height);
		var fonts = getFonts();
		var center = r.items[0];
		val font = when (center.input.size) {
			FontSize.Big -> fonts.bigFont;
			FontSize.Small -> fonts.smallFont;
			FontSize.SuperSmall -> fonts.superSmallFont;
			else -> throw IllegalArgumentException();
		}
		g.font = font
		g.color = Color.black
		g.drawString(center.input.text, center.x, center.y + font.size);
		println("w ${width} h ${height} x ${x} y ${y}")
		//println("center ${center.x} ${center.y}")
		for (i in 1 until r.items.size)
		{
			var item = r.items[i];
			g.font = fonts.smallFont
			g.drawString(item.input.text,item.x, item.y + fonts.smallFont.size);
			println("s$i ${item.x} ${item.y}")
		}
	}


}
