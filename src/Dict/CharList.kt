package wthumbs

import java.nio.file.Files
import java.nio.file.Paths
import java.util.*

object CharList
{
	val filePath = "res/Dict/charlist.txt"
	val hash = HashSet<Char>()

	init{
		val line = Files.readAllLines(Paths.get(filePath),Charsets.UTF_8)[0]
		for(c in line)
			hash.add(c)
	}

	fun isWordChar(c : Char) : Boolean
	{
		return hash.contains(c);
	}
}