//package wthumbs
//
//class MyDictionary
//{
//	const string FilePath = "Dict/mydictionary.txt";
//
//	public List<MyDictionaryItem> Items;
//
//	public MyDictionary()
//	{
//		Items = LoadItems();
//	}
//
//	public List<string> Merge(string[] original)
//	{
//		var list = new List<string>(original);
//		var set = new HashSet<string>(original);
//
//		foreach (var item in Items)
//		{
//			if (set.Contains(item.Word))
//			{
//				list.Remove(item.Word);
//			}
//			double power = 999999 - item.Freq;
//			power /= 999999;
//			int index = (int)(list.Count * power);
//
//			if (index < 0)
//			{
//				index = 0; //多分起こらない
//			}
//			else if (list.Count <= index)
//			{
//				index = list.Count - 1;
//			}
//			list.Insert(index, item.Word);
//		}
//		return list;
//	}
//
//	/// <summary>
//	/// AddしてSortしてSaveする。
//	/// </summary>
//	public void Add_Sort_Save(string word, int freq)
//	{
//		_Add(word, freq);
//		_Sort();
//		_Save();
//	}
//
//	/// <summary>
//	/// SortもSaveもしないでAddだけする。
//	/// 大量追加でパフォーマンスに問題が出る場合を想定している。多分いらないけど。
//	/// Sortしないと正しい結果にならないので注意
//	/// </summary>
//	public void _Add(string word, int freq)
//	{
//		Items.RemoveAll(item => item.Word == word);
//		Items.Add(new MyDictionaryItem { Word = word, Freq = freq });
//	}
//
//	public void _Sort()
//	{
//		//安定ソート
//		Items = Items.OrderBy(item => item).ToList();
//	}
//
//	public void _Save()
//	{
//		File.WriteAllLines(FilePath, Items.Select(item => item.ToString()).ToArray());
//	}
//
//	public void ClearSave()
//	{
//		Items.Clear();
//		_Save();
//	}
//
//	public static List<MyDictionaryItem> LoadItems()
//	{
//		var lines = File.ReadAllLines(FilePath);
//		var list = new List<MyDictionaryItem>();
//
//		foreach (var line in lines)
//		{
//			var item = MyDictionaryItem.ParseN(line);
//			if (item != null)
//			{
//				list.Add(item);
//			}
//		}
//		return list;
//	}
//}
//
//class MyDictionaryItem : IComparable<MyDictionaryItem>
//{
//	const int Max = 999999;
//	int _Freq;
//	/// <summary>
//	/// 000000-999999まで
//	/// </summary>
//	public int Freq
//	{
//		get { return _Freq; }
//		set
//		{
//			if (value < 0)
//			{
//				_Freq = 0;
//			}
//			else if (Max < value)
//			{
//				_Freq = Max;
//			}
//			else { _Freq = value; }
//		}
//	}
//	public string Word;
//
//	/// <summary>
//	/// Freqが小さい順に並べる
//	/// </summary>
//	public int CompareTo(MyDictionaryItem other)
//	{
//		return this.Freq - other.Freq;
//	}
//
//	public override string ToString()
//	{
//		return $"{Word} {Freq:000000}";
//	}
//
//	/// <summary>
//	/// word 999999という書式
//	/// </summary>
//	public static MyDictionaryItem ParseN(string line)
//	{
//		if (8 <= line.Length)
//		{
//			int freq;
//			if (int.TryParse(line.Substring(line.Length - 6, 6), out freq))
//			{
//				return new MyDictionaryItem
//						{
//							Freq = freq,
//							Word = line.Substring(0, line.Length - 7)
//						};
//			}
//		}
//		return null;
//	}
//}